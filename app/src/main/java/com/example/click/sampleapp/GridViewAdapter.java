package com.example.click.sampleapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;



public class GridViewAdapter extends BaseAdapter {

    Context context;
    ArrayList<Student> result;
    LayoutInflater inflater;

    public GridViewAdapter(Context context, ArrayList<Student> data) {
        this.context=context;
        this.result=data;
        this.inflater=LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyGridViewHolder holder = new MyGridViewHolder();
        View rowView;


        rowView = inflater.inflate(R.layout.grid, null);
        holder.tvName=(TextView) rowView.findViewById(R.id.txt_grid_name);
        holder.tvRollNo=(TextView) rowView.findViewById(R.id.txt_grid_roll_no);

        Student myStudent= (Student) getItem(position);

        holder.tvRollNo.setText(String.valueOf(myStudent.getRollNo()));
        holder.tvName.setText(myStudent.getName());


        return rowView;

    }

    private class MyGridViewHolder {

        TextView tvName, tvRollNo;

        }

    public ArrayList<Student> getGridStudent() {
        return result;
    }
}
