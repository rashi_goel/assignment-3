package com.example.click.sampleapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends Activity {

    //UI Controls
//    ViewStub listStudentDetails, gridStudentDetails;
    private ListView list;
    private GridView grid;
    Button btnCreateStudent;
    Spinner btnSpinner;
    ToggleButton toggleView;

    //Variables
    String name;
    int rollNo;
    ListAdapter customListAdapter;
    GridViewAdapter customGridAdapter;


    public static final int CREATE_STUDENT_REQUEST = 1;
    showAlert dialog;


    private String[] searchOption= {" ","By Name", "By Roll No"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setting out the main view
        setContentView(R.layout.activity_main);
//
//        //Extracting the Ids of UI controls on the layout
//        listStudentDetails = (ViewStub) findViewById(R.id.list_student_details);
//        gridStudentDetails = (ViewStub) findViewById(R.id.grid_student_details);
//
//        //Inflate the layouts
//        listStudentDetails.inflate();
//        gridStudentDetails.inflate();

        //Setting out layouts for the list/grid
        list = (ListView) findViewById(R.id.list_student_details);
        grid = (GridView) findViewById(R.id.grid_student_details);

        //Extracting the ID's of button and Toggle button
        btnCreateStudent = (Button) findViewById(R.id.btn_add_student);
        btnSpinner= (Spinner) findViewById(R.id.spinner);
        toggleView = (ToggleButton) findViewById(R.id.toggleButton);

        grid.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);

        //Button Click Listener on 'Create Student' Button
        btnCreateStudent.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Starting the second activity for a result
                Intent studentDetails = new Intent(getApplicationContext(), DetailsActivity.class);
                startActivityForResult(studentDetails, CREATE_STUDENT_REQUEST);

            }
        });


        customListAdapter = new ListAdapter(MainActivity.this, new ArrayList<Student>());
        customGridAdapter= new GridViewAdapter(MainActivity.this, new ArrayList<Student>());

        list.setAdapter(customListAdapter);
        grid.setAdapter(customGridAdapter);


        //Setting the adapter to spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, searchOption) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    tv.setVisibility(View.GONE);
                    v = tv;
                }
                else {
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }

                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        btnSpinner.setAdapter(spinnerAdapter);

        //Adding OnItem Click Listener to Spinner Button
        btnSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //btnSpinner.setPrompt("Sort");
                //btnSpinner.setSelection(position);
                //String selState = (String) btnSpinner.getSelectedItem();

                if (position==1) {

                    /*Sorting based on Student Name*/
                    Collections.sort(customListAdapter.getListStudent(), Student.StuNameComparator);
                    Collections.sort(customGridAdapter.getGridStudent(), Student.StuNameComparator);
                    customListAdapter.notifyDataSetChanged();
                    customGridAdapter.notifyDataSetChanged();
                }

                else if (position==2)
                {
                    /*Sorting based on Student Roll No*/
                    Collections.sort(customListAdapter.getListStudent(), Student.StuRollno);
                    Collections.sort(customGridAdapter.getGridStudent(), Student.StuRollno);
                    customListAdapter.notifyDataSetChanged();
                    customGridAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Setting OnClickListener to list item for displaying dialog

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Toast.makeText(MainActivity.this, "" + position, Toast.LENGTH_SHORT).show();

                    dialog = new showAlert();
                    FragmentManager fragmentManager = getFragmentManager();
                    dialog.show(fragmentManager, "frag_name");


                }
            });

        //Setting onClickListener to the grid item for displaying dialog

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Toast.makeText(MainActivity.this, "" + position, Toast.LENGTH_SHORT).show();

                dialog = new showAlert();
                FragmentManager fragmentManager = getFragmentManager();
                dialog.show(fragmentManager, "frag_name");

            }
        });



        //Setting the OnClickListener For Toggle Button
        toggleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleView.isChecked()) {

                    //Show Gridview

                    if(list.getVisibility()== View.VISIBLE)
                    {
                        list.setVisibility(View.GONE);
                        grid.setVisibility(View.VISIBLE);
                       /* customGridAdapter = new GridViewAdapter(MainActivity.this, new ArrayList<Student>());
                        grid.setAdapter(customGridAdapter);
*/

                    };

                }
                else
                {
                    //Show listview
                        grid.setVisibility(View.GONE);
                        list.setVisibility(View.VISIBLE);
                }
                    /*customListAdapter = new ListAdapter(MainActivity.this, new ArrayList<Student>());
                    list.setAdapter(customListAdapter);*/

                }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_STUDENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Inside onActivityResult()", Toast.LENGTH_SHORT).show();

                //Getting the data from the intent
                name = data.getExtras().getString("name");
                rollNo = data.getExtras().getInt("roll_no");

                System.out.println("Name in MainActivity is : " + name);
                System.out.println("Roll No in MainActivity is :" + rollNo);


                Student student = new Student(rollNo, name);
                student.setName(name);
                student.setRollNo(rollNo);

                //Getting the list from the listAdapter/gridViewAdapter and adding the student successively
                customListAdapter.getListStudent().add(student);
                customGridAdapter.getGridStudent().add(student);


                //Setting the custom adapter for listview/gridview
                customListAdapter.notifyDataSetChanged();
                customGridAdapter.notifyDataSetChanged();

            }
            if (requestCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Error! List not populated!", Toast.LENGTH_SHORT).show();
            }
        }

    }


    //Method For Displaying the Dialog on a Row Click
    public static class showAlert extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            setRetainInstance(true);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Message");
            builder.setMessage("What operation you want to perform?");
            builder.setPositiveButton("View", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub

                    //Code For View Operation

                }
            });
            builder.setNeutralButton("Edit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub

                    //Code For Edit Operation

                }
            });


            builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    //Code For Delete Operation

                }
            });
            return builder.create();

        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }


}

