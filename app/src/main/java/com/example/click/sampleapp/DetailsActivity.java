package com.example.click.sampleapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by click on 30/06/15.
 */
public class DetailsActivity extends Activity {

    EditText edtStudentName, edtStudentRollNo;
    Button btnOk, btnClear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);

        edtStudentName= (EditText) findViewById(R.id.edt_name);
        edtStudentRollNo= (EditText) findViewById(R.id.edt_roll_no);
        btnOk= (Button) findViewById(R.id.btn_ok);
        btnClear= (Button) findViewById(R.id.btn_clear);


        btnOk.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

               boolean flag= CheckAllFields();
                if(flag==true)
                {
                    Intent returnIntent = new Intent(getApplicationContext(), MainActivity.class);
                    returnIntent.putExtra("name",edtStudentName.getText().toString());
                    returnIntent.putExtra("roll_no", Integer.parseInt(edtStudentRollNo.getText().toString()));
                    setResult(RESULT_OK, returnIntent);
                    returnIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                }


            }
        });

    }

    public boolean CheckAllFields()

    {

        if(!(edtStudentRollNo.getText().toString().length()>0))
        {
            edtStudentRollNo.setError("roll no is Required!");
            return false;

        }
        else if(!(edtStudentName.length()>0))
        {
            edtStudentName.setError(" name is Required!");
           return false;

        }

        else
        {

            return true;
        }
    }

}
