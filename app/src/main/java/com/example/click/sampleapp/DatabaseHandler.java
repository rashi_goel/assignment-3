package com.example.click.sampleapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static java.lang.String.valueOf;

/**
 * Created by click on 13/07/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="studentManager";
    private static final String TABLE_NAME="student";

    //private static final String KEY_ID="_id";
    private static final String KEY_ROLL_NO="Rollno";
    private static final String KEY_NAME="Name";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_STUDENT="CREATE TABLE "+TABLE_NAME+KEY_ROLL_NO+" TEXT PRIMARY KEY, "+KEY_NAME+"TEXT"+")";
        db.execSQL(CREATE_TABLE_STUDENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS"+TABLE_NAME);
        onCreate(db);

    }


    //Adding a new student
    public void addStudent(Student student)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values= new ContentValues();
        values.put(KEY_ROLL_NO, student.getRollNo());
        values.put(KEY_NAME, student.getName());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }


    //Getting a student
    public Student getStudent(int id)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_NAME, new String[]{ KEY_ROLL_NO, KEY_NAME},KEY_ROLL_NO+"=?",new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor!=null)

            cursor.moveToFirst();

        Student student= new Student(Integer.parseInt(cursor.getString(0)), cursor.getString(1));

        return student;
    }

    public List<Student> getAllStudents()
    {
        List<Student> listStudent= new ArrayList<Student>();
        String selectQuery="Select * from "+TABLE_NAME;
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor cursor=db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst())
        {
            do {
                Student student= new Student();
                student.setRollNo(Integer.parseInt(cursor.getString(0)));
                student.setName(cursor.getString(1));
                listStudent.add(student);
            }
            while (cursor.moveToNext());
        }
        return listStudent;
    }



}
